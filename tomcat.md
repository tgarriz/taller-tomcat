# Taller Tomcat

## Que es Tomcat y para que sirve?

Es un servidor de aplicaciones web gratuito y de código abierto que implementa la plataforma Java Enterprise Edition (Java EE). Es un contenedor que se utiliza para ejecutar aplicaciones web Java.
Tiene la capacidad de asignar recursos como la memoria, el procesamiento y a las aplicaciones contenidas.
Tomcat es un servidor de aplicaciones ligero y eficiente que es ideal para aplicaciones web de tamaño pequeño a mediano. También es un buen punto de partida para las aplicaciones web de gran tamaño.

Aquí hay algunos de los beneficios de usar Tomcat:
- Es gratuito y de código abierto.
- Es ligero y eficiente.
- Es fácil de usar y configurar.
- Es compatible con una amplia gama de tecnologías Java EE.
- Es una herramienta popular y bien documentada.

Si está buscando un servidor de aplicaciones web Java, Tomcat es una excelente opción. Es una herramienta poderosa y confiable que puede ayudarlo a crear y ejecutar aplicaciones web Java de manera efectiva.

## Requerimientos

Los requisitos para instalar Tomcat en Linux son los siguientes:
- Un sistema operativo Linux compatible.
- Un Java Runtime Environment (JRE) instalado.
- Espacio de almacenamiento disponible en el disco duro.
- Una cuenta de usuario con permisos de escritura en el directorio de instalación de Tomcat.

## Instalacion de Tomcat en un entorno Linux Debian 12

### Instalar Java

```bash
sudo apt update
sudo apt install openjdk-17-jdk
```
### Tomcat debe ejecutarse como un usuario sin privilegios para mayor seguridad.  Para comenzar, ejecute el siguiente comando para crear el usuario y el grupo de Tomcat
```bash
sudo groupadd tomcat
sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
```
### Descargar el tomcat del sitio oficial https://archive.apache.org/dist/tomcat/

Establecer el versionado y ejecutar los siguientes comandos
```bash
cd /tmp
rm -f apache-tomcat-*.tar.gz 
VER="9.0.8"
wget https://archive.apache.org/dist/tomcat/tomcat-9/v${VER}/bin/apache-tomcat-${VER}.tar.gz
```

### Crear directorio y descomprimir
```bash
sudo mkdir /opt/tomcat
sudo tar xzvf apache-tomcat-*.tar.gz -C /opt/tomcat --strip-components=1
```
### Setear los permisos necesarios

```bash
cd /opt/tomcat
sudo chgrp -R tomcat /opt/tomcat
```
### Setear permisos de lectura, escritura y ejecucion
```bash
sudo chmod -R g+r conf
sudo chmod g+x conf
```
### Asignar dueño a los directorios
```bash
sudo chown -R tomcat webapps/ work/ temp/ logs/
```
### Creación del service

Crear el file /etc/systemd/system/tomcat.service

```bash
[Unit]
Description=Apache Tomcat Web Application Container
After=network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/default-java
Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/tomcat
Environment=CATALINA_BASE=/opt/tomcat
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/tomcat/bin/startup.sh
ExecStop=/opt/tomcat/bin/shutdown.sh

User=tomcat
Group=tomcat
UMask=0007
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
```

### Actualizar los daemons e inicializar el tomcat
```bash
sudo systemctl daemon-reload
sudo systemctl enable --now tomcat
```

### Habilitar usuario admin
Ubicar el file /opt/tomcat/conf/tomcat-users.xml para habilitar los roles y setear contraseña
```bash
<tomcat-users> 
...........

<role rolename="admin"/>
<role rolename="admin-gui"/>
<role rolename="manager"/>
<role rolename="manager-gui"/>

<user username="admin" password="YourPassword" roles="admin,admin-gui,manager,manager-gui"/>

</tomcat-users> 
```
### Desabilitar  Administración por acceso remoto
Edite el file /opt/tomcat/webapps/host-manager/META-INF/context.xml, ubique el siguiente string y comentelo
```bash
<!-- <Valve className="org.apache.catalina.valves.RemoteAddrValve"
allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" /> -->
```
Luego edite /opt/tomcat/webapps/manager/META-INF/context.xml, y realice la misma accion
```bash
<!-- <Valve className="org.apache.catalina.valves.RemoteAddrValve"
allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" /> -->
```

### Reinicar el tomcat

```bash 
systemctl restart tomcat
```

### Instalar nginx para realizar un proxy reverso
Nginx es un servidor HTTP, un servidor proxy inverso y un equilibrador de carga de código abierto, muy rápido y eficiente. Está escrito en C y se ejecuta en la mayoría de los sistemas operativos.
Nginx se utiliza a menudo como servidor proxy inverso para Tomcat. Esto significa que Nginx acepta solicitudes del cliente y luego las enruta a Tomcat. Esto puede mejorar el rendimiento y la seguridad de su aplicación web.

Algunas de las ventajas de usar Nginx como proxy inverso para Tomcat incluyen:

- Mejor rendimiento: Nginx es más rápido que Tomcat, lo que puede mejorar el rendimiento de su aplicación web.
- Mejor seguridad: Nginx puede proporcionar una capa adicional de seguridad para su aplicación web al filtrar solicitudes maliciosas.
- Fácil de configurar: Nginx es fácil de configurar y puede ser configurado para satisfacer sus necesidades específicas.
- Nginx puede servir contenido estático desde el disco duro, lo que puede liberar recursos en Tomcat.
- Nginx puede servir contenido de múltiples servidores web, lo que puede ayudar a escalar su aplicación web.

### Instalar nginx

```bash
apt install nginx
```

### Configurar nginx 
Editar la configuracion en el file correspondiente
/etc/nginx/conf.d/tomcat.conf

```bash
server {
  listen          80;
  server_name     tomcat.test.com;

  location / {
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8080/;
  }
}
```
### Reiniciar nginx

```bash
systemctl restart nginx
```

###
